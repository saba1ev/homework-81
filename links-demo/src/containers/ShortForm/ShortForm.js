import React, {Component} from 'react';
import {Button, Container, Input, Label} from "reactstrap";
import './ShortForm.css'
import {connect} from "react-redux";
import {fetch_link} from "../../store/actions";

class ShortForm extends Component {
  state = {
    originalUrl: ''
  };
  changeHendler = (event) =>{
    this.setState({[event.target.name]: event.target.value})
  };
  render() {
    return (
      <Container>
        <h2 className='TitleLink'>Shorten your Link!</h2>

        <Label className='FormLink'>
          <div>Press your url</div>
          <Input type='text' name='originalUrl' value={this.state.originalUrl} onChange={(event)=>this.changeHendler(event)}/>
          <Button color='primary' onClick={()=>this.props.postLink(this.state)}>Shorten</Button>

        </Label>
        {this.props.link.shortUrl ? <div>
          <h1>Your short link</h1>
          <a href={this.props.link.originalUrl}>http://localhost:8000/{this.props.link.shortUrl}</a>
        </div> : null}
      </Container>
    );
  }
}
const mapStatetoProps = state =>{
  return{
    link: state.link
  }
};
const mapDispatchToProps = dispatch =>{
  return{
    postLink: (linkData) => dispatch(fetch_link(linkData))
  }
};

export default connect(mapStatetoProps, mapDispatchToProps) (ShortForm);