import {FETCH_LINK_FAILURE, FETCH_LINK_REQUEST, FETCH_LINK_SUCCESS} from "./ActionTypes";
import axios from '../axios-link'

export const fetch_link_request = () =>{
  return{type: FETCH_LINK_REQUEST}
};
export const fetch_link_success = link =>{
  return{type: FETCH_LINK_SUCCESS, link}
};
export const fetch_link_failure = fail =>{
  return{type: FETCH_LINK_FAILURE, fail}
};

export const fetch_link = (linkData) =>{
  return dispatch =>{
    dispatch(fetch_link_request());
    axios.post('/links', linkData).then(response=>{
      dispatch(fetch_link_success(response.data))
    }, error=>{
      dispatch(fetch_link_failure(error))
    })
  }
}