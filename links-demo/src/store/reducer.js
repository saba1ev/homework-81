import {FETCH_LINK_FAILURE, FETCH_LINK_REQUEST, FETCH_LINK_SUCCESS} from "./ActionTypes";

const initialState = {
  link: [],
  error: null
};

const reducer = (state = initialState, action) =>{
  switch (action.type) {
    case FETCH_LINK_REQUEST:
      return {...state};
    case FETCH_LINK_SUCCESS:
      return{...state, link: action.link};
    case FETCH_LINK_FAILURE:
      return{...state, error: action.error};
    default:
      return state
  }
};

export default reducer;