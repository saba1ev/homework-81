import React, {Component, Fragment} from 'react';
import {Switch, Route} from 'react-router-dom';
import ShortForm from "./containers/ShortForm/ShortForm";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Switch>
          <Route path='/' exact component={ShortForm}/>
        </Switch>
      </Fragment>
    );
  }
}

export default App;