const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const link = require('./app/dbLinks');

const app = express();
app.use(cors());
app.use(express.json());


const port = 8000;
mongoose.connect('mongodb://localhost/shortUrl', {useNewUrlParser: true}).then(() =>{
  app.use('/links', link);
  app.listen(port, ()=>{
    console.log('We are use, ' + port)
  });
});
