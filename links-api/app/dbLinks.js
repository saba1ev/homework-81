const express = require('express');
const nanoid = require('nanoid');

const link = require('../model/links');

const router = express.Router();

router.get('/:shortUrl', (req, res)=>{
  link.findOne({shortUrl: req.params.shortUrl}).then(url =>{
    if (url){
      return res.status(301).redirect(link.originalUrl);
    } else return res.status(404).send('not found');

  }).catch(err =>{
    res.status(400).send(err)
  });
});

router.post('/', (req, res)=>{
  console.log(req.body);
  const shortUrl = new link({
    originalUrl: req.body.originalUrl,
    shortUrl: nanoid(6)
  });
  shortUrl.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

module.exports = router;