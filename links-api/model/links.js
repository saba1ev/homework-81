const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const linksSchema = new Schema ({
  shortUrl:{
    type: String,
    required: true
  },
  originalUrl: {
    type: String,
    required: true
  }
});
const links = mongoose.model('links', linksSchema);
module.exports = links;
